<?php get_header();?>
<div class="main">
	<div class="single-post-cnt">
		<?php  
		if (have_posts()) :
			while (have_posts()) : the_post(); ?>
			<!-- target 3pages contactus to incert menu start-->
			<?php if(is_page('contact-us') || is_page('by-email') || is_page('by-phone')) { ?>
					 <ul class="contact-us-ep-menu">
						<?php 
							if($post->post_parent){
							$args = array(
							'child_of'=>$post->post_parent,
							'title_li'=>null
							);
							}
							else{
							$args = array(
							'child_of'=>$post->ID,
							'title_li'=>null);
							}
							?>
						    <?php wp_list_pages($args) 
						?>﻿
				      </ul>
			     <?php }?>
			     <!-- target 3pages contact us to incert menu end -->
			<article class="the-post">
				<h3><?php the_title(); ?></h3>
				<?php the_content(); ?>
				<?php echo $post->menu_order; ?>
			</article>
		<?php endwhile;
		else :?>
				<h3 style="text-align: center;"><?php __('No page found') ?></h3>
				<?php endif;
			?>
		<div class="bottom-logo-cnt">
				<a href="<?php echo home_url();?>"><span id="logo"  class=" bottom-logo"></span></a>
		</div>
    </div>
</div>
<?php get_footer();?>
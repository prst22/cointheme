<?php
get_header();
?>
<div class="main margin">
	<div class="home-cnt">
		<?php
			if (have_posts()) :
				while (have_posts()) : the_post(); ?>
					<article class="post">
						<div class="inner-post-cnt">
							
							<!-- time category author start  end-->
							<?php if(has_post_thumbnail()): ?>
								<?php if(get_post_format() == 'video'){ ?>
									<div class="post-thumnail">
										<a href="<?php the_permalink() ?>">
										    <span class="icon-font coin-film"></span>
										    <?php the_post_thumbnail('medium-thumnail');?>
									    </a>
                                    </div>
								<?php } else { ?>
								<div class="post-thumnail">
									<a href="<?php the_permalink() ?>">
										<?php the_post_thumbnail('medium-thumnail');?>
									</a>
								</div>
								
							<?php } endif; ?>

							<h4 class="post-header bold <?php if(!has_post_thumbnail()): ?> no-padding <?php endif; ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
							<!-- time category author start -->
							<div class="post-info bold">
								 Posted in <?php $categories = get_the_category(); 
							$separetor = ", ";
							$output = '';
							if($categories){
								foreach($categories as $category){
									$output .= '<a href="'. get_category_link($category->term_id) . '">' . $category ->cat_name . '</a>' .$separetor;
								}
								echo trim($output, $separetor);
							}
							?>
								<?php the_time('F j, Y'); ?>
								<?php edit_post_link(
										sprintf(
											/* translators: %s: Name of current post */
											__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'cointheme' ),
											get_the_title()
										),
										'<span class="edit-link-p">',
										'</span>'
									); ?> 
							</div>
                       
							<div class="post-excerpt bot-line">
								<?php 
									
									the_excerpt();
									
								?>
							</div>
					    </div>
					</article>
				<?php endwhile;?>


				<?php
				
			else :
				echo '<h3 style="text-align: center;">No posts so far( </h3>';
			endif;
		?>
		<span class="bottom-fence"></span>
	</div>

</div>

<div class="pagination-cnt">
	<div class="inner-pag">
		<?php echo paginate_links(array(
			'next_text' => '<span class="right-read-more pagination-arrow"></span>',
			'prev_text' => '<span class="left-read-more pagination-arrow"></span>'

			));?>
	</div>
</div> 

<?php		
	get_footer();
?>
<footer class="footer">
	<div class="about">
		<h4>About</h4><p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	consequat.</p></div>
	<?php if(get_theme_mod('mfwt-footer-display') == "Yes"){?>
		<div class="footer-callout">
			<div class="footer-callout-image"><img src="<?php echo wp_get_attachment_url(get_theme_mod('mfwt-footer-image')) ?>"></div>

			<div class="footer-callout-text">
				<h3><a href="<?php echo get_permalink(get_theme_mod('mfwt-footer-link')) ?>"><?php echo get_theme_mod('mfwt-footer-heading') ?></a></h3>
				<p><?php echo get_theme_mod('mfwt-footer-text') ?>
				</p>		
			</div>
		</div>
	<?php } ?>
	<nav class="footer-menu">
		<?php
		$args = array(
			'theme_location' => 'footer'
		);
		 wp_nav_menu($args); ?>

		<p class="footer-copy"><?php bloginfo('name') ?> - &copy; <?php echo date("Y");?>	
	    </p>
	</nav>

	    <?php  if(is_active_sidebar('footer-search')){ 
	            dynamic_sidebar('footer-search');
            }
	    ?>	

	
</footer>
<?php wp_footer(); ?>
</body>

</html>

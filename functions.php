<?php
	//add theme support
	function add_q() {
		// Adding menus to wp controll pannel
		register_nav_menus(array(
			'primary' =>__('Primary Menu'),
			'footer' =>__('Footer Menu')
		));

	    //post thum pictures
	    add_theme_support( 'post-thumbnails', array( 'post', 'custom-type'));
	    add_image_size('small-thumnail', 180, 90, array('center','center'));
	    add_image_size('medium-thumnail', 500, 250, array('center','center'
	    ));  

	    add_theme_support( 'post-formats', array( 'video' ));
	    //CUSTOM WORDPRESS EDITOR STYLE
		add_editor_style( 'css/custom-editor-styles.css' );
	    //CUSTOM WORDPRESS EDITOR STYLE 
	    
	}

	add_action('after_setup_theme', 'add_q');
	//adding css styles

	function addStyles(){
		wp_enqueue_style('style', get_theme_file_uri( '/css/main.css'));
		wp_enqueue_style('style2', get_theme_file_uri( '/css/style.css'));
		wp_enqueue_script('font_js', get_template_directory_uri() . '/js/fontfaceobserver.standalone.js', array(), 1.0, false);
		wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.js', array(), 1.0, true);
		if (is_single() && get_post_format() == 'video') {
			wp_enqueue_script('video_js', get_template_directory_uri() . '/js/video.js', array(), 1.0, false);
		} 
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
	add_action('wp_enqueue_scripts', 'addStyles');

	//Controll post length on the main page

	function set_excerpt_length(){
		if( is_page('exchanges-ranking')){
			return has_post_thumbnail() ? 20 : 40;
		}else{
			return has_post_thumbnail() ? 30 : 55;
		} 
	}
	add_filter('excerpt_length','set_excerpt_length');

	function custom_excerpt_read_more( $more ) {
	    return sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
	        get_permalink( get_the_ID() ),
	        __( '<span class="right-read-more"></span>', 'textdomain' )
	    );
	}
	add_filter( 'excerpt_more', 'custom_excerpt_read_more' );

	//widgets locations start
	function wpb_init_widgets($id){
		register_sidebar(array(
		    'name'=>'footerSearch',
		    'id'=> 'footer-search',
		    'before_widget'=> '<div class="footer-search">',
		    'after_widget'=> '</div>'
		));
	}
	add_action('widgets_init', 'wpb_init_widgets');
	//widgets locations end

	//search input start
    function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
	}
	add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );
    //search input end

	//include walker class

	require_once('walker.php');

	// Add responsive container to embeds
 
	function alx_embed_html( $html ) {
	   return (is_single() && get_post_format() == 'video') ? 
	   '<div class="screen"><div class="video-container">' . $html . '</div></div>' :'<div class="video-container">' . $html . '</div>';
	}
	 
	add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );
	// removing url field in comments section start
	function disable_url_in_comments_field($fields){
		if(isset($fields['url']))
		unset($fields['url']);
		return $fields;
	}
    add_filter('comment_form_default_fields', 'disable_url_in_comments_field');
    // removing url field in comments section end

?>

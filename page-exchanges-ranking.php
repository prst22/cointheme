<?php get_header();?>
<div class="main">
	<div class="single-post-cnt">
		<?php  
		if (have_posts()) :
			while (have_posts()) : the_post(); ?>
				<article class="the-post">
					<h3><?php the_title(); ?></h3>
					<table>
					  <tr>
					    <th><a href="#" title="<?php esc_attr_e('книга', 'truemisha') ?>"><?php _e('Terms and Conditions', 'truemisha') ?></a></th>
					    <th>Contact</th>
					    <th>Country</th>
					  </tr>
					  <tr>
					    <td>Alfreds Futterkiste</td>
					    <td>Maria Anders</td>
					    <td>Germany</td>
					  </tr>
					  <tr>
					    <td>Centro comercial Moctezuma</td>
					    <td>Francisco Chang</td>
					    <td>Mexico</td>
					  </tr>
					  <tr>
					    <td>Ernst Handel</td>
					    <td>Roland Mendel</td>
					    <td>Austria</td>
					  </tr>
					  <tr>
					    <td>Island Trading</td>
					    <td>Helen Bennett</td>
					    <td>UK</td>
					  </tr>
					  <tr>
					    <td>Laughing Bacchus Winecellars</td>
					    <td>Yoshi Tannamuri</td>
					    <td>Canada</td>
					  </tr>
					  <tr>
					    <td>Magazzini Alimentari Riuniti</td>
					    <td>Giovanni Rovelli</td>
					    <td>Italy</td>
					  </tr>
					  <tr>
					    <td>Alfreds Futterkiste</td>
					    <td>Maria Anders</td>
					    <td>Germany</td>
					  </tr>
					  <tr>
					    <td>Centro comercial Moctezuma</td>
					    <td>Francisco Chang</td>
					    <td>Mexico</td>
					  </tr>
					  <tr>
					    <td>Ernst Handel</td>
					    <td>Roland Mendel</td>
					    <td>Austria</td>
					  </tr>
					  <tr>
					    <td>Island Trading</td>
					    <td>Helen Bennett</td>
					    <td>UK</td>
					  </tr>
					  <tr>
					    <td>Laughing Bacchus Winecellars</td>
					    <td>Yoshi Tannamuri</td>
					    <td>Canada</td>
					  </tr>
					  <tr>
					    <td>Magazzini Alimentari Riuniti</td>
					    <td>Giovanni Rovelli</td>
					    <td>Italy</td>
					  </tr>
					</table>

					<!-- custom query with pagination start-->

				        <?php
		                     $currentPage = get_query_var('paged');  
			                 $businessPostsVariable = new WP_Query(array(
			                 	'category_name'=>'business',
			                 	'posts_per_page'=> 4,
			                 	'paged'=> $currentPage

			                 ));
                         
		                 if($businessPostsVariable->have_posts()): ?>
		                 <div class="recent-post-cnt"> 
		                 	<span style="display: block; font-size: 1rem;">Last business posts</span>
		                 	    <div class="inner-recent-post-cnt">
				                    <?php while($businessPostsVariable->have_posts()): 
				                    	 $businessPostsVariable->the_post();?>
				                            <div class="recent-post">
				                            	<?php if(has_post_thumbnail()): ?>
													<div class="post-thumnail">
														<a href="<?php the_permalink() ?>">
															<?php the_post_thumbnail('medium-thumnail');?>
														</a>
													</div>
													
												<?php endif; ?>
						                    	<a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?></h5>
						                    	</a>
					                    	<?php the_excerpt(); ?></div>	
				                    
				                     <?php endwhile;?>
				                     </div>
		                 </div>
		                 <?php endif; 
		                 wp_reset_postdata();
		                 ?> 

					<!-- custom query with pagination end -->

				</article>
		<?php endwhile;
		else :?>
				<h3 style="text-align: center;">'<?php __('No page found') ?></h3>
				<?php endif;
			?>
		<div class="bottom-logo-cnt">
				<a href="<?php echo home_url();?>"><span id="logo"  class=" bottom-logo"></a>
		</div>
    </div>
</div>
<?php get_footer();?>
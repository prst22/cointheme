<div class="inner-single-post">
	<div class="inner-single-post-cnt">
		<h4 class="post-header bold"><?php the_title(); ?></h4>
		<!-- time category author start -->
		<?php if(is_single()){?> <div class="post-info padding bold"><?php }else{?>
	        <div class="post-info">
			<?php } ?>
				 Posted in <?php $categories = get_the_category(); 
			$separetor = ", ";
			$output = '';
			if($categories){
				foreach($categories as $category){
					$output .= '<a href="'. get_category_link($category->term_id) . '">' . $category ->cat_name . '</a>' .$separetor;
				}
				echo trim($output, $separetor);
			}
			?>
			<?php the_time('F j, Y'); ?>
			<?php edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'cointheme' ),
					get_the_title()
				),
				'<span class="edit-link-p">',
				'</span>'
			); ?> 
			</div>
		<!-- time category author start  end-->

		<div class="video-post-content">
			<?php 
				the_content();
			?>
		</div>
    </div>
</div>
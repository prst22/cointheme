!function(){
	let menu = document.getElementById("menu-primary");
	let subDropTrigger = document.getElementsByClassName("dropdown-toggle");
	let subDrop = document.getElementsByClassName("dropdown-menu");
	let logoTrigger = document.getElementById("home-url");
	let logo = document.getElementById("logo");
	let rotation = 0;
	let pElementsCnt = document.querySelectorAll(".post-content>p");
	logoTrigger.addEventListener("mouseover", function(){
		rotation+=360;
		logo.style.transform = "rotate(" + rotation + "deg)"; 
	});
	// controll font loading start
	let font1 = new FontFaceObserver('PT Sans Narrow', {
		  weight: 400
	});
	let font2 = new FontFaceObserver('PT Sans Narrow', {
		  weight: 700
	});
	let html = document.documentElement;

	html.classList.add('fonts-loading');

	Promise.all([font1.load(null, 5000), font2.load(null, 5000)]).then(function () {
	  html.classList.remove('fonts-loading');
	  html.classList.add('fonts-loaded');
	  sessionStorage.fontsLoaded = true;
	}).catch(function () {
	  html.classList.remove('fonts-loading');
	  html.classList.add('fonts-failed');
	  sessionStorage.fontsLoaded = false;
	});
	// controll font loading start end
	document.getElementById("btn").onclick = function() {
			if(!(menu.classList.contains("show"))) {
				menu.classList.add("show");
				menu.classList.remove("hide");
			}
			else {
				menu.classList.remove("show");
				menu.classList.add("hide");
			}
		}
	window.onclick=function (x){
			if(!x.target.matches("#menu-primary * ") 
				&& !x.target.matches("#btn") 
				&& !x.target.matches(".line")){
				menu.classList.remove("show");
				menu.classList.add("hide");
		}
		if(!x.target.matches(".menu-item-has-children * ")){
			subDrop[0].classList.remove("show-sub-drop");
			subDrop[0].classList.add("hide-sub-drop");
		}
	}
	subDropTrigger[0].addEventListener("click", function(e){

		if(subDrop){
			if(!(subDrop[0].classList.contains("show-sub-drop"))) {
					subDrop[0].classList.add("show-sub-drop");
					subDrop[0].classList.remove("hide-sub-drop");
					e.preventDefault();
				}
				else {
					subDrop[0].classList.remove("show-sub-drop");
					subDrop[0].classList.add("hide-sub-drop");
					e.preventDefault();
				}
			}
	});
	let matchMe = window.matchMedia("(max-width: 900px)"); 
	matchMe.addListener(closeMenuOnResize);
	function closeMenuOnResize(){
		if(matchMe.matches || !matchMe.matches){
			if(menu && menu.classList.contains("show")) {
				menu.classList.add("hide");
				menu.classList.remove("show");
			}
			if(subDrop && subDrop[0].classList.contains("show-sub-drop")) {
				subDrop[0].classList.add("hide-sub-drop");
				subDrop[0].classList.remove("show-sub-drop");
			}
		}
	}
	if (Element && !Element.prototype.matches){
	    var proto = Element.prototype;
	    proto.matches = proto.matchesSelector ||
	        proto.mozMatchesSelector || proto.msMatchesSelector ||
	        proto.oMatchesSelector || proto.webkitMatchesSelector;
	}
	function addClassToFirstPwithText(){
		let bool = true;
		for (let e = 0; e < pElementsCnt.length; e++) {
		    if (pElementsCnt[e] && bool === true) {    	
				searchForTextInP(e);
			}else{
				break;
			}
		}
	    function searchForTextInP(e){
			let children = pElementsCnt[e].childNodes;
				for (let i = 0; i < children.length; i++) {
				  	if (children[i].nodeType === 3 && children[i].nodeValue != "") {
				  		let amountOfWords = WordCount(children[i].nodeValue);
				  		if(amountOfWords >= 25){
				  			pElementsCnt[e].classList.add("first-p-with-text");
				            bool = false;
				            break;   
				  		}else{
				  			bool = false;
				  			break;
				  		}
			            
			        }
			    }
		}
	}
	function WordCount(str) { 
	  return str.split(" ").length;
	}

	if(pElementsCnt){
	    addClassToFirstPwithText();
	}
	if (/MSIE 10/i.test(navigator.userAgent)) {
		document.documentElement.style.backgroundColor = "#262626";
	}
	if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
	    document.documentElement.style.backgroundColor = "#262626";
	}
	 if (/Edge\/\d./i.test(navigator.userAgent)){
	    document.documentElement.style.backgroundColor = "#262626";
	}
	
	let link = document.querySelectorAll("#menu-primary>li>a");
	let loadingEl = document.getElementsByClassName("loading-el");
	if (link) {
		document.addEventListener("DOMContentLoaded", function(event){
		 
			for(i = 0; i < link.length; i++){
				link[i].removeChild(loadingEl[0]);
			}
			
		});
	}
	document.addEventListener('touchstart', function addtouchclass(e){ // first time user touches the screen
    document.documentElement.classList.add('can-touch') // add "can-touch" class to document root using classList API
    document.removeEventListener('touchstart', addtouchclass, false) // de-register touchstart event
	}, false)

}();

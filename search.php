<?php
/*
Template Name: Search Page
*/
?>
<?php
	get_header();
?>
<div class="main margin">
	<div class="post-cnt column">
		<div id="search-form-cnt"><?php get_search_form(); ?></div>
		<h3 style="padding: 0 1.5rem">Search results</h3>
		<?php
			if (have_posts()) :
				while (have_posts()) : the_post(); ?>
					<article class="search-rez-post">
						<div class="search-post-cnt">
							<h4 class="post-header bold"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
							<!-- time category author start -->
							<div class="post-info bold">
								 Posted in <?php $categories = get_the_category(); 
							$separetor = ", ";
							$output = '';
							if($categories){
								foreach($categories as $category){
									$output .= '<a href="'. get_category_link($category->term_id) . '">' . $category ->cat_name . '</a>' .$separetor;
								}
								echo trim($output, $separetor);
							}
							?>
								<?php the_time('F j, Y'); ?>
							</div>
							<!-- time category author start  end-->
							<?php if(has_post_thumbnail()): ?>
								<?php if(get_post_format() == 'video'){ ?>
									<div class="small-post-thumnail">
										<a href="<?php the_permalink() ?>">
										    <span class="icon-font coin-film"></span>
										    <?php the_post_thumbnail('small-thumnail');?>
									    </a>
                                    </div>
                                    <?php } else { ?>
								<div class="small-post-thumnail">
									<a href="<?php the_permalink() ?>">
										<?php the_post_thumbnail('small-thumnail');?>
									</a>
								</div>
								
							<?php } endif; ?>

							<div class="post-excerpt">
								<?php 
									
									the_excerpt();
									
								?>
							</div>
					    </div>
					</article>
				<?php endwhile;?>


				<?php
				
			else :
				echo '<h3 style="text-align: center;">No matches found( </h3>';
			endif;
		?>
	</div>

</div>

<div class="pagination-cnt">
	<div class="inner-pag">
		<?php echo paginate_links(array(
			'next_text' => '<span class="right-read-more pagination-arrow"></span>',
			'prev_text' => '<span class="left-read-more pagination-arrow"></span>'

			));?>
	</div>
</div> 

<?php		
	get_footer();
?>


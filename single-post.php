<?php
get_header();
?>
<div class="main">
	<div class="single-post-cnt">
		<?php 
			if (have_posts()) :
			while (have_posts()) : the_post(); ?>
				<?php get_template_part('content', get_post_format()); ?>
				
			<?php endwhile;
		    else : ?>
			<h3 style="text-align: center;"><?php __('No post found') ?></h3>
			<?php endif;
		?>
		<div class="bottom-logo-cnt">
				<a href="<?php echo home_url();?>"><span id="logo"  class="bottom-logo"></span></a>
		</div>
		<?php
			if ( comments_open() || get_comments_number() ) :?>
			<div class="comment-cnt">
			     <?php comments_template(); ?>
			</div>
			 <?php endif;
		?>
    </div>
     
</div>
<?php		
get_footer();
?>
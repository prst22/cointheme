<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
    <meta name="description" content="<?php bloginfo('description'); ?>">
	<title>
	    <?php bloginfo('name');?> |
	    <?php is_front_page() ? bloginfo('description'): 
        wp_title('');
	    ?> 
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Igor Barabash">
    <meta name="keywords" content="custom WordPress theme">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon" sizes="32x32">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" type="image/x-icon" sizes="32x32">
	<?php wp_head();  ?>
	<script type="text/javascript">
		if (sessionStorage.fontsLoaded) {
			var html = document.documentElement;
			html.classList.add('fonts-loaded');
		}
	</script>
</head>
<body <?php body_class();?>>
	<header><h1><a href="<?php echo home_url();?>"><span id="logo"  class="icon-coin"></span></a><a href="<?php echo home_url();?>" id="home-url" class="transition"></a></h1></header>

	<nav class="nav top-line">

		<div id="btn" class="btn1">
				<div class="line"></div>
				<div class="line"></div>
				<div class="line"></div>
			
		</div>

		<?php
           wp_nav_menu( array(
               'menu'              => 'primary',
               'theme_location'    => 'primary',
               'menu_class'        => 'menu',
               'depth'             => 2,
               'container'         => NULL,
               'item_spacing'      => 'discard',
               'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
               'walker'            => new wp_bootstrap_navwalker())
           );
        ?>
		
	</nav>
<?php
	get_header();
?>
<div class="main">
	<div class="post-cnt column" style="justify-content: center; min-height: inherit;">
		<div id="search-form-cnt" style="width: 100%;"><?php get_search_form(); ?></div>
		<h3 style="text-align: center; width: 100%;">Something went wrong. Please try again</h3>
		<h1 style="text-align: center; font-weight: 700; width: 100%;">404</h1>
    </div>
</div>
<?php		
	get_footer();
?>
